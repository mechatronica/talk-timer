# Talk Timer

## Normal use

- Download `timer.html` or clone the repository.
- Open `timer.html` in your browser
- The timer fills the screen.
- To restart the timer, reload the page (`F5` for most browsers)

## Changing the duration

- Open `timer.html`
- On line 13, change the amount of time in milliseconds.